﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

suspendScript := "C:/Users/CosmicAnimeCat/Documents/Scripts/suspend-program.ps1"

#SingleInstance

isSuspended := false

; Suspend
$F8::
if (isSuspended == true)
	Return

isSuspended := true

TempFile := A_Temp . "\pid.temp"
FileDelete, %TempFile%

RunWait, powershell.exe "%suspendScript%" >> %TempFile%,, hide

FileRead, PID, %TempFile%
FileDelete, %TempFile%

Return

; Resume
$F9::
if (isSuspended == false)
	Return

isSuspended := false

RunWait, powershell.exe -command PsSuspend -r "%PID%",, hide

Return
