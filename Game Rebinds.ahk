#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

; Match title exactly
SetTitleMatchMode 3
#SingleInstance

; Tower of fantasy
#IfWinActive ahk_exe QRSL.exe

$XButton1::m
$XButton2::Tab

; Honkai Star Rail
#IfWinActive ahk_exe StarRail.exe

$XButton1::m

; FF XIV
#IfWinActive ahk_exe ffxiv_dx11.exe

$XButton1::;
;$XButton1::Ctrl
;$XButton2::Alt

#IfWinActive

$F10::Reload
$F12::Suspend
